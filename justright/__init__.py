# python3
""" General help(MODULE) documentation

This is where you'd put documentation to be shown when a user does
something like::

    import justright
    help(justright)

This is also a good place to put license information

"""
# The "# SPDX" line below indicates what software license this is released under.
# If you're not releasing it under a license, remove it. If you are, check
# https://spdx.org/licenses/ for a suitable identifier
# SPDX-License-Identifier: INVALID-YOU-NEED-TO-REMOVE-THIS-OR-SET-IT

__version__ = "0.1.0"

def doubler(x):
    """ Return the value of x, doubled

    This is really just an example of documentation visible if a user 
    does something like

        import justright
        help(justright.doubler)

    Here are some examples that can be tested with doctest:

    >>> from justright import doubler
    >>> doubler(3)
    6
    >>> doubler("pom")
    'pompom'
    """
    return x*2

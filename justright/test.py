#! /usr/bin/python3
""" test.py documentation
"""
# SPDX-License-Identifier: INVALID-YOU-NEED-TO-REMOVE-THIS-OR-SET-IT


import unittest

class SomeTest(unittest.TestCase):
    def test_doubler(self):
        from justright import doubler
        self.assertEqual(doubler(10), 20)
        self.assertEqual(doubler("me"), "meme")

def load_tests(loader, tests, ignore):
    """ Also run doctest in other files """
    import doctest
    import justright
    tests.addTests(doctest.DocTestSuite(justright))
    return tests


if __name__ == '__main__':
    unittest.main()

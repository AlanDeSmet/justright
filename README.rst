justright
=========

A "just right" tutorial and working example for publishing a module on PyPi.or
and readthedocs.io. It strives to be a middle ground between tutorials and
templates that are too minimal to use in practice, and the `full documentation
<https://packaging.python.org/tutorials/packaging-projects/>` which can be
overwhelming.

This requires Python 3.6 or newer. It also requires various command line tools
typical on Linux systems: GNU Make, GNU find, GNU xargs, touch, echo, and rm.


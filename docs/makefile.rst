make tools
==========

At the top of the package is a ``Makefile``, configuration for GNU make to perform a variety of packaging steps.  Options include

- **make test** - Runs the test suite using ``python3 -m justright.test``.

- **make clean** - Remove all generated files.

- **make tag-version** - Adds a git tag based on the version found in your module. Ignore or modify to your needs if you're using a different version control system.

- **make build** - Create the package in the ``dist/`` subdirectory. Output form the build tool will bein ``build.log`` as the build tool is pretty noisy.

- **make test-local-install** - Depends on ``make build``. Will try to install the locally built package in a virtual environment and run tests in the same way as ``make test``.

- **make release-to-test** - Depends on ``make build``. Publish your package to
  TestPyPi.  This re-does everything from the start, cleaning up, running the
  test, building the package, testing the created package, publishing the
  package to TestPyPi, and ultimately running ``make test-remote-install``.

- **make test-remote-install** - Depends on ``make release-to-test``. Will try
  to install the package from TestPyPi into a virtual environment and run tests
  in the same way as ``make test``.

- **make release** - TODO

A variety of the tasks will automatically create `virtual environments <https://docs.python.org/3/library/venv.html>`_ to ensure they have all required tools as well as providing an isolated location for testing created packages. There directories will all end in ``-env``. They are cleaned up by ``make clean``.

During development, you might regularly run ``make test`` to ensure tests are passing. Quick local builds can be done and tested with ``make test-local-install``. When ready to release use ``make release-to-test``; assuming it doesn't fail use ``make tag`` to tag your release.

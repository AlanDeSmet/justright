API Documentation
=================

This is the automatically generated documentation for the ``justright`` module.
It's not very useful as part of the tutorial, but it's here as an example if
you customize this into your own package.

.. automodule:: justright
   :members:

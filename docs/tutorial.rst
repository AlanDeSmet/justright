
Publishing Python Packages
==========================

Prerequisites
-------------

This assumes you have a Python 3.6 or later module in one or more 
self-contained ``.py`` files and that you want to publish it as a package on
`PyPi.org <https://pypi.org/>`_
and possibly also publish documentation at
`Read the Docs <https://readthedocs.io>`_.
Publishing on PyPi assumes you also want to release your module for use by
others, and thus have a suitable license in mind.

For the sake of this tutorial, we'll assume you're shipping a single module named ``MYMODULE`` (although Python modules are typically all lowercase).

You'll need a package name. You can use ``MYMODULE``, although appending your name or PyPi username is recommended to avoid conflicts, so we'll go with ``MYMODULE-MYUSERNAME``.

This assumes you're on Linux or a system that behaves similarly to Linux, and
that you have various command line tools common on Linux systems: GNU Make, GNU
find, GNU xargs, touch, echo, and rm.

If you want to publish documentation on Read the Docs, you will need your
package to exist in a version control repository on the public internet. As of
2021, Read the Docs supports Git, Mercurial, Bazaar, and Subversion. You can
run your own or use an existing provider like `GitLab <https://gitlab.com>`_ or
`GitHub <https://github.com/>`_.



Create PyPi and Read the Docs Accounts
--------------------------------------

To publish your package to PyPi, you'll need to
`register for an account at PyPi <https://pypi.org/account/register/>`_
*and*
`register for an account at TestPyPi <https://test.pypi.org/account/register/>`_.
You do want both so you can use TestPyPi to make sure the process is working
correctly before publishing for real on PyPi.

To publish your documentation on Read the Docs, you'll need 
`register for an account at Read The Docs <https://readthedocs.org/accounts/signup/>`_.



Customize This Template
-----------------------

You can use the justright repository as a template for your own project::

    $ git clone -depth 1 https://gitlab.com/AlanDeSmet/justright.git
    $ # Disconnect from the justright repository
    $ rm -rf justright/.git

Now customize it for your package::

    $ # Rename the package
    $ mv justright MYMODULE
    $ # Rename the module
    $ mv MYMODULE/justright MYMODULE/MYMODULE

You'll now need to update ``setup.cfg``. You can edit it manually or get
some help from ``customize``::

    $ customize

``customize`` will prompt you for your package name, module name, author name or pseudonym, author email (optional), brief description, various web pages (home, documentation, and issue tracker. all optional), and license, offering the best defaults it can. Once finished, it will re-write ``setup.cfg`` appropriately. (You can re-run ``customize``, but if ``setup.cfg`` has been manually modified, it may mangle it. If you're happy with ``setup.cfg`` and with later doing updates yourself, you can remove ``customize``.))



TODOTODOTODO

- Setting up ReadTheDocs



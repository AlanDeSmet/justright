justright: An Opinionated Tutorial and Template for Publishing Python Packages
==============================================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   tutorial
   template-contents
   q-and-a
   makefile
   apidocs



justright tutorial and template for publishing Python 3.6 and
later packages on PyPi.org and readthedocs.io. 
It strives to be a middle ground between tutorials and
templates that are too minimal to use in practice, and the `full documentation
<https://packaging.python.org/tutorials/packaging-projects/>`_ which can be
overwhelming. It strives to be "Just Right."

This requires Python 3.6 or newer. It also requires various command line tools
typical on Linux systems: GNU Make, GNU find, GNU xargs, touch, echo, and rm.




Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`




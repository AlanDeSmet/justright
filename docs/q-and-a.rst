Questions and Answers
=====================

What do I do if my module is just a single file?
------------------------------------------------

If your module is a single file (for example ``mymodule.py``), you'll need to
move it to a ``__init__py`` file inside of a directory (for example
``mymodule/__init__.py``). Various parts of the packaging system make bare
modules tricky to deal with. It's possible, but you're on your own.

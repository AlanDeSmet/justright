VERSION:=$(shell ./build-util get module-version)
PACKAGE_NAME:=$(shell ./build-util get package-name)
MODULE_NAME:=$(shell ./build-util get module-name)

TARBALL:=$(shell ./build-util get dist-tarball)
EGG_DIR:=$(shell ./build-util get egg-directory)
UNNEEDED_FILES:=$(EGG_DIR) __pycache__ dist build-env test-local-env test-remote-env build.log


all: 
	make clean
	make test
	make build
	make test-local-install

validate:
	./build-util validate

.PHONY: all clean test build tag-version
.PHONY: test-local-install test-remote-install release release-to-test


################################################################################

clean: 
	rm -rf $(UNNEEDED_FILES)
	find -type d -name __pycache__ -print0 | xargs -0 rm -rf 


################################################################################

test:
	python3 -m $(MODULE_NAME).test

################################################################################

tag-version:
	git tag "$(VERSION)"
	echo "You will probably want to "
	echo "    git push origin $(VERSION)"

################################################################################

%-env:
	python3 -m venv "$@"
	"$@"/bin/pip3 --quiet install --upgrade pip

################################################################################

build: $(TARBALL)

$(TARBALL): build-env/ready
	build-env/bin/python3 -m build > build.log

build-env/ready: | build-env
	build-env/bin/pip3 --quiet install --upgrade build
	build-env/bin/pip3 --quiet install --upgrade twine
	touch $@

################################################################################

test-local-install: $(TARBALL) | test-local-env
	test-local-env/bin/pip3 --quiet install dist/anyrange_AlanDeSmet-*-py3-none-any.whl
	# Work in a subdirectory so we don't accidentally use the working copy
	(cd test-local-env && bin/python3 -m anyrange.test)

################################################################################

test-remote-install: dist/released-to-test | test-remote-env
	test-remote-env/bin/pip3 --quiet install --index-url https://test.pypi.org/simple/ --no-deps $(PACKAGE_NAME)
	# Work in a subdirectory so we don't accidentally use the working copy
	(cd test-remote-env && bin/python3 -m anyrange.test)


################################################################################

release-to-test: 
	make clean
	make test
	make build
	make test-local-install
	make dist/released-to-test
	make test-remote-install

dist/released-to-test: $(TARBALL)
	build-env/bin/python3 -m twine upload --repository testpypi dist/*
	touch $@

release: 
	echo "You might try"
	echo "build-env/bin/python3 -m twine upload --repository testpypi dist/*"

